//
//  PopularTableViewCell.swift
//  GameCatalogue
//
//  Created by Muhamad Widi Aryanto on 14/07/20.
//  Copyright © 2020 Muhamad Widi Aryanto. All rights reserved.
//

import UIKit

class PopularTableViewCell: UITableViewCell {
    @IBOutlet weak var imagePopular: UIImageView!
    @IBOutlet weak var titlePopular: UILabel!
    @IBOutlet weak var rankingPopular: UILabel!
    @IBOutlet weak var releaseDatePopular: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
