//
//  NewReleaseTableViewCell.swift
//  GameCatalogue
//
//  Created by Muhamad Widi Aryanto on 14/07/20.
//  Copyright © 2020 Muhamad Widi Aryanto. All rights reserved.
//

import UIKit

class NewReleaseTableViewCell: UITableViewCell {
    @IBOutlet weak var imageNewRelease: UIImageView!
    @IBOutlet weak var titleNewRelease: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
